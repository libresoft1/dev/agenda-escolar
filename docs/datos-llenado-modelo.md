## Llenado de datos

Ensayo de formatos de archivos para que las Unidades educativas (U.E.) puedan llenar datos.

## Módulo de asistencia

**Datos oblitatorios**, opcionales.

###  Estudiantes (propuesta 1)

Para cargar datos de estudiantes se espera un archivo .csv u hoja de cálculo con los siguientes datos:

- **Nombre**
- **Ap. Paterno**
- **Ap. Materno**
- **grado**
- **Sexo**
- Edad
- **Apoderado1 nombres**
- **Apoderado1 ap. paterno**
- **Apoderado1 ap. materno**
- **Apoderado1 CI**
- Apoderado1 email
- Apoderado1 telf
- Apoderado2 nombres
- Apoderado2 ap. paterno
- Apoderado2 ap. materno
- Apoderado2 CI
- Apoderado2 email
- Apoderado2 telf
- Apoderado3 nombres
- Apoderado3 ap. paterno
- Apoderado3 ap. materno
- Apoderado3 email
- Apoderado3 telf

Ejemplo:

| Nombre | Ap. Paterno | Ap. Materno | Grado | Sexo | Edad | Apoderado1 nombre | Apoderado1 ap. paterno | Apoderado1 ap. materno | Apoderado1 CI | Apoderado1 telf | Apoderado1 email | Apoderado2 nombres | Apoderado2 ap. paterno | Apoderado2 ap. materno | Apoderado2 CI | Apoderado2 telf | Apoderado2 email |
|---|---|---|---|---|---|---|---|---|----|---|---|---|---|----|----|---|----|
| Juan Carlos  | Guzmán   | Rinoa  | 1 secundaria  | Masculino  | 16  | Martha  | Rinoa  | Saile  | 878751 | 70707070 | jc@alc.com | Ernesto   | Guzmán  | Selna  | 7747171 | 7080808 | sem.ce@gmail.com |
| María  | Sudañes   | Chambi  | 6 primaria  | Femenio  | 14  | Lucía  | Chambi  | Telles  | 90000 | 224141 | |  Albert   | Sudañes  | Estevez  | 8848181 | 881801 | fer.ad@gmail.com |
| Roberta  | Mijae   | Adriani  | 6 primaria  | Femenio  | 14  | Roberta  | Adriani  | Marcov | 32232332  | | | Marco | Miaje  | Torres  | 90919191 | 581851 | |

### Profesores y cursos (propuesta 2 - Lau)

Para cargar datos de profesores y cursos se espera un archivo .csv u hoja de cálculo con los siguientes datos:

- **Nombre completo** (del profesor)
- **CI**
- **Cargo**
- **Materia**
- **Nivel**
- **Grado**
- **Paralelo**
- Turno
- Email

| Nombre completo | CI | Cargo | Materia | Nivel | Grado | Paralelo | Turno | Email | Telf |
|-----------------|----|-------|---------|-------|-------|----------|-------|-------|------|
| Luariña Hernández Perez| 448181 | Maestra | Física | Secundaria | 1 | A | Mañana | larunia@tones.com | 8851881 |
| Alfrediño Ramirez Duarte| 8680235 | Maestro | Matemáticas | Primaria | 1 | B | Mañana | amgerd.duarte@algo.com | 9996161 |

### Tabla Padres o tutores

- **Nombre**
- **Ap. Paterno**
- **Ap. Materno**
- **CI**
- **Sexo**
- **Nombre de persona a cargo**
- **Ap. Paterno de persona a cargo**
- **Ap. Materno de persona a cargo**
